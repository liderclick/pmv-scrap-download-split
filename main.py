import json
from scraper.pornhub import Pornhub
from scraper.video import Video
from scraper.clips import clips_pornhub_hotspots as Clips

from download.download import Download

from moviepy.editor import *
import os

total_seg = 0
i = 1

######################
#####   inputs   #####
######################
_url = str(input("URL: "))
_cant = int(input("Cantidad Videos: "))
_fullSecciones = int(input("Seleccines Completas - [0]No  [1]Si : "))
if _fullSecciones == 0:
   _titleSecctions = str(input("Titulo Secciones (Dir): "))
   _time = int(input("Tiempo Todas las Secciones (Seg): "))
   pass

_descargar = int(input("Descargar Videos - [0]No  [1]Si : "))

info = Pornhub(_url,_cant)
# info = Pornhub('https://es.pornhub.com/video?c=138&o=mv&t=m&max_duration=20',1)
# info = Pornhub('https://es.pornhub.com/video?o=mv&t=m',1)


# Create folders
if not os.path.exists("media"):
   os.mkdir("media")
   os.mkdir('media/videos')
   os.mkdir('media/clips')

path_media_videos = "media/videos/"
if not os.path.exists(path_media_videos):
   os.mkdir(path_media_videos)

path_media_clips = "media/clips/"
if not os.path.exists(path_media_clips):
   os.mkdir(path_media_clips)


for info_video in info.videos:
   videoTitle = str(info_video.title).replace('|', ' - ').strip()
   videoId = str(info_video.id)

   # VIDEOS
   path_video = path_media_videos+videoId
   if not os.path.exists(path_video):
      os.mkdir(path_video)
   
   with open(path_video+'/'+videoTitle+'.json', 'w') as f:
      f.write(info_video.toJSON())

   # Download Video
   if _descargar == 1:
      video_download = Download(info_video.url, path_video, videoTitle)
      video_download.start()
   pass
   
   # CLIPS
   path_base_clips = path_media_clips+videoId
   if not os.path.exists(path_base_clips):
      os.mkdir(path_base_clips)
   
   with open(path_base_clips+'/'+videoTitle+'.json', 'w') as f:
      f.write(info_video.toJSON())

   # Search Secciones
   sections = Clips(info_video.hotspots, info_video.duration)
   secciones = []
   secciones_avg = sections.get_sections_avg()
   
   if _fullSecciones == 0:
      secciones_time = sections.get_sections_time(secciones_avg, _time)
      path_clip = path_base_clips+'/'+_titleSecctions+' - '+str(_time)+' seg'
      if not os.path.exists(path_clip):
         os.mkdir(path_clip)
      sections.print_secctions(secciones_time)
      secciones = secciones_time
      pass
   else:
      path_clip = path_base_clips
      sections.print_secctions(secciones_avg)
      secciones = secciones_avg
      pass    


   # PARTES
   for seccion in secciones:
      total_seg += int(seccion['time'])

      if _descargar == 1:
         path_file_clip = path_clip+"/"+str(i)+'-'+str(seccion['index'])+".mp4"
         if not os.path.isfile(path_file_clip):
            clip = VideoFileClip(path_video+"/"+videoTitle+".mp4").subclip(seccion['star'],seccion['end'])
            clip.write_videofile(path_file_clip)
         else:
            print(str(i)+'-'+str(seccion['index'])+".mp4 | File exist!")
      pass

   i+=1   


print('TOTAL DE VIDEOS: '+str(len(info.videos)))
print('TOTAL DE TIEMPO: '+str(total_seg / 60)+' Min')
