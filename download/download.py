import youtube_dl as dl

class Download():

    def __init__(self, link, path, title):
        self.link = link
        self.path = path
        self.title = title
        self.config = {
            'format': 'bestvideo+bestaudio/best',
            'noplaylist': True,
            'postprocessors': [{
                'key': 'FFmpegVideoConvertor',
                'preferedformat': 'mp4',
                #'preferredquality': '137',
            }],
            'outtmpl': path+'/'+title+'.mp4',
        }


    def start(self):
        try:
            with dl.YoutubeDL(self.config) as ydl:
                ydl.download([self.link])
        except dl.utils.DownloadError as err:
                print("DownloadError Occurred !!!")
                print(err)


if __name__ == '__main__':
    link = 'https://www.youtube.com/watch?v=YV89HBHc4AA'
    D = Download(link, 'Demo')
    D.start()
