# Import everything needed to edit video clips
from moviepy.editor import *

# my_clip.write_videofile("demo.mp4") # Will fail ! NO DURATION !
# my_clip.set_duration(5).write_videofile("demo_edit.mp4") # works !

# Load myHolidays.mp4 and select the subclip 00:00:50 - 00:00:60
clip = VideoFileClip("demo.mp4").subclip(20,30)

# Reduce the audio volume (volume x 0.8)
clip = clip.volumex(0.8)

# Overlay the text clip on the first video clip
video = CompositeVideoClip([clip])
# video = CompositeVideoClip([clip])

# Write the result to a file (many options available !)
video.write_videofile("demo_edit.mp4")