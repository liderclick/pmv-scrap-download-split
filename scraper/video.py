import json
from .pornhub_scraper import video_metadata

class Video():
   
   def __init__(self, url_video):
      self.id = ''
      self.url = url_video
      self.title = ''
      self.duration = 0
      self.hotspots = []
      self.categories = []
      self.tags = []
      self.views = ''
      self.votes_up = ''
      self.votes_down = ''
      
      self.get_data()

   def get_data (self):
      metadata = video_metadata(self.url)
      # for timestamp, thumbnail in video_thumbnails(metadata):
      #     thumbnail.save(str(timestamp)+'.jpg')
      self.id = metadata['id']
      self.title = metadata['title']
      self.duration = metadata['duration']
      self.hotspots = metadata['hotspots']
      self.categories = metadata['categories']
      self.tags = metadata['tags']
      self.views = metadata['views']
      self.votes_up = metadata['votes_up']
      self.votes_down = metadata['votes_down']
      pass

   def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, 
            sort_keys=True, indent=4)